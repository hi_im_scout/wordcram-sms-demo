import wordcram.*;
import javax.swing.JFileChooser;
import java.io.File;   

private WordCram wordCram;
private PFont archd;
private PImage bg;
private color darkPurple = color(81, 45, 168);
private color blue = color(33, 150, 243);

void setup() {
  size(1366, 768);
  archd = createFont("/SpecialElite.ttf", 48);
  bg = loadImage("/gplaypattern.png");
  tilebg();
  init(getFileName());
  
  System.out.println("Starting draw...");
  wordCram.drawAll();
  
  Word[] skipped = wordCram.getSkippedWords();
  for(Word w : skipped) {
    System.out.println(w.toString());
  }
  System.out.println(skipped.length + " words were not placed");
}

String getFileName() {
  JFileChooser fileChooser = new JFileChooser();
  fileChooser.setCurrentDirectory(new File(System.getProperty("user.home")));
  int result = fileChooser.showOpenDialog(null);
  
  if (result == JFileChooser.APPROVE_OPTION) {
    File selectedFile = fileChooser.getSelectedFile();
    return selectedFile.getAbsolutePath();
  } else {
    System.exit(0);
    return null;
  }
}

void tilebg() {
  System.out.println("Loading bg...");
  for(int i = 0; i < width + bg.width; i += bg.width) {
    for(int j = 0; j < height + bg.height; j += bg.height) {
      image(bg, i, j);
    }
  }
}

void init(String fileName) {
  System.out.println("file: " + fileName);
  System.out.println("Initializing wordcloud params...");
  wordCram = new WordCram(this).
    fromTextFile(fileName).
    sizedByWeight(15,100).
    keepCase().
    withFont(archd).
    minShapeSize(7).
    //withPlacer(Placers.swirl()).
    withStopWords("2/2 1/2 3/4 2/3 1/3 3/3 4/4 2/4 1/4").
    withColors(blue, darkPurple, color(25));
}