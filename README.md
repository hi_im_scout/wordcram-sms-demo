# WordCram SMS Demo #

This is a quick demo of using the [WordCram](http://wordcram.org/) Processing library with SMS conversation data. It was written alongside my [Android SMS Exporter](https://bitbucket.org/alex_newkirk/android-sms-to-txt-exporter) to visualize word frequency in text chats. 